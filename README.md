# Get LORe from supplementary table

Script to get the atlantic salmon duplicated regions (homeolog blocks) from the supplementary table of the Atlantic salmon genome article https://doi.org/10.1038/nature17164 and mark the lineage specific ohnolog resolution (LORe) regions.

## dupRegionTbl.tsv

This table is the same as the supp.tbl. 6 but with the column names changed (more scripting friendly) and an additional column "isLORe" which marks the LORe regions.

First five rows:

| blockNr|dupBlockName |chr_x |  start_x|    end_x|chr_y |  start_y|    end_y|direction |isLORe |
|-------:|:------------|:-----|--------:|--------:|:-----|--------:|--------:|:---------|:------|
|       1|1p-9qa_1     |ssa01 |        0|  6843616|ssa09 | 43662329| 48507357|-         |FALSE  |
|       2|1p-9qa_2a    |ssa01 |  6953722|  8602432|ssa09 |  4747739|  6656450|+         |FALSE  |
|       3|1p-9qa_x     |ssa01 |  8768777|  9096203|ssa09 |   154418|   269552|-         |FALSE  |
|       4|1p-9qa_2b    |ssa01 |  9166964| 44457745|ssa09 |  6778154| 43618303|+         |FALSE  |
|       5|1p-9qa_2c    |ssa01 | 44474054| 45500129|ssa09 |  3643423|  4716301|-         |FALSE  |

## Ssal_LORe.tsv

Simplified table of LORe regions. I.e. no direction and some blocks are merged.

Entire table:

|chr   |    start|      end|LOReRegion |
|:-----|--------:|--------:|:----------|
|ssa02 |        0| 37183267|2p-5q      |
|ssa02 | 37237908| 72943711|2q-12qa    |
|ssa03 | 53345225| 92503428|3q-6p      |
|ssa04 |        0| 24741121|4p-8q      |
|ssa05 | 44403485| 80503876|2p-5q      |
|ssa06 |        0| 44497429|3q-6p      |
|ssa07 | 30107113| 58350292|7q-17qb    |
|ssa08 |        0| 26045199|4p-8q      |
|ssa11 |  4847401| 44157154|11qa-26    |
|ssa12 |        0| 31357447|2q-12qa    |
|ssa16 | 58876430| 87796322|16qb-17qa  |
|ssa17 |  1168892| 31267708|16qb-17qa  |
|ssa17 | 31983542| 57585316|7q-17qb    |
|ssa26 |  4517345| 47898159|11qa-26    |